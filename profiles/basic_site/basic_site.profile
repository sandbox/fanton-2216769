<?php
/**
 * @file
 * Basic site installation profile custom functions.
 */

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Allows the profile to alter the site configuration form.
 */
function basic_site_form_install_configure_form_alter(&$form, $form_state) {
  // Populate variables.
  $profilename = drupal_get_profile();
  $function = $profilename . '_profile_details';
  if (function_exists($function)) {
    $details = $function();
    // Pre-populate the site name with the server name.
    $form['site_information']['site_name']['#default_value'] = $details['site_name'];
    $form['site_information']['site_mail']['#default_value'] = $details['site_mail'];
    $form['admin_account']['account']['name']['#default_value'] = $details['name'];
    $form['admin_account']['account']['mail']['#default_value'] = $details['mail'];
    $form['server_settings']['date_default_timezone']['#default_value'] = 'America/Montevideo';
    $form['update_notifications']['update_status_module']['#default_value'] = array(1);
  }
}

/**
 * Allow profile to pre-select the language, skipping the selection.
 */
function basic_site_profile_details() {
  // Default language
  $details['language'] = "es";

  // Path
  $details['pathauto_node_pattern'] = '[node:title]';
  $details['pathauto_update_action'] = 2;
  $details['pathauto_ignore_words'] = 'a, an, as, at, before, but, by, for, from, is, in, into, like, of, off, on, onto, per, since, than, the, this, that, to, up, via, with, como, con, de, del, el, en, es, la, lo, por, que, sin, y';
  $details['pathauto_transliterate'] = TRUE;

  // Set timezone for date_timezone.
  $details['date_format_short'] = 'd/m/Y';
  $details['date_format_medium'] = 'd/m/Y - H:i';
  $details['date_format_long'] = 'D, d/m/Y - H:i';
  $details['site_default_country'] = 'UY';

  // Alter configuration form.
  $details['site_name'] = $_SERVER['SERVER_NAME'];
  $details['site_mail'] = 'info@' . $_SERVER['SERVER_NAME'];
  $details['name'] = 'admin' . $_SERVER['SERVER_NAME'];
  $details['mail'] = 'info@' . $_SERVER['SERVER_NAME'];

  // Disable personal contact form.
  $details['contact_default_status'] = '0';

  // Set the new user creation to administrators.
  $details['user_register'] = '0';

  // Disable user pictures.
  $details['user_pictures'] = '0';

  // JQuery version 1.7.
  $details['jquery_update_jquery_version'] = '1.7';

  // Cache options.
  $details['cache'] = '1';
  $details['block_cache'] = '1';
  $details['cache_lifetime'] = '300';
  return $details;
}
